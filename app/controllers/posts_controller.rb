require 'apns'
require 'gcm'

class PostsController < ApplicationController
  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @posts }
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @post = Post.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/new
  # GET /posts/new.json
  def new
    @post = Post.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @post }
    end
  end

  # GET /posts/1/edit
  def edit
    @post = Post.find(params[:id])
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(params[:post])
    
    android_registration_ids = []
  	ios_registration_ids = []
  	
  	# Collect all devices' tokens
  	Device.all.each do |device|
  		if device.system == "android"
  			android_registration_ids << device.token
  		else # iOS
    		ios_registration_ids << device.token
  		end
		end
		
		# Android Notification
		
		gcm = GCM.new("AIzaSyD6toaexYzMySpzuIdyKZSnnb65ESgNiJ4")
		options = {data: {message: @post.body}, collapse_key: "updated_message"}
		gcm.send_notification(android_registration_ids, options)
    
    # iOS Notification
    
		APNS.pem = Rails.root.join("files", "cert.pem")
		APNS.host = 'gateway.push.apple.com'
    APNS.feedback_host = 'feedback.push.apple.com'
    
    # APNS.feedback.each do |feedback|
    #   d = Device.find(:first, :conditions => { :token = feedback.device_token })
    #   unless d.nil?
    #     d.feedback_on = feedback.feedback_on
    #   end
    # end
    
    APNS.pass = ''
		notifications = []
		ios_registration_ids.each do |token|
		  notifications << [token, :aps => { :alert => @post.body, :sound => 'default' }]
		end
		APNS.send_notifications(notifications)
		
    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render json: @post, status: :created, location: @post }
      else
        format.html { render action: "new" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /posts/1
  # PUT /posts/1.json
  def update
    @post = Post.find(params[:id])

    respond_to do |format|
      if @post.update_attributes(params[:post])
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end
end
